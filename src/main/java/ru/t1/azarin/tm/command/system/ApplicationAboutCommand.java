package ru.t1.azarin.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public final static String NAME = "about";

    @NotNull
    public final static String ARGUMENT = "-a";

    @NotNull
    public final static String DESCRIPTION = "Display developer info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println(getServiceLocator().getPropertyService().getAuthorName());
        System.out.println(getServiceLocator().getPropertyService().getAuthorEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
