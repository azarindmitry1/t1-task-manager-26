package ru.t1.azarin.tm.api.model;

import org.jetbrains.annotations.NotNull;

public interface IHasName {

    @NotNull
    String getName();

    @NotNull
    void setName(String name);

}