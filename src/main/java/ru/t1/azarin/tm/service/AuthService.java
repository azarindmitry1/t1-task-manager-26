package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.service.IAuthService;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.IUserService;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.exception.entity.UserNotFoundException;
import ru.t1.azarin.tm.exception.field.LoginEmptyException;
import ru.t1.azarin.tm.exception.field.PasswordEmptyException;
import ru.t1.azarin.tm.exception.user.AccessDeniedException;
import ru.t1.azarin.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.azarin.tm.exception.user.PermissionException;
import ru.t1.azarin.tm.exception.user.UserIsLockedException;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private String userId;

    public AuthService(@NotNull final IPropertyService propertyService, @NotNull final IUserService userService) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @NotNull
    @Override
    public User registry(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.isLocked()) throw new UserIsLockedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
